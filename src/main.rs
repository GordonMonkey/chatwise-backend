use actix_web::{App, HttpServer, Responder};
use actix_cors::Cors;
use actix_web::web::Data; 
use chrono::Utc;
use serde_json::Value;
use sqlx::Row;
use sqlx_postgres::{PgQueryResult, PgRow};
use nano_id::base64;
use actix::{Actor, Addr, AsyncContext, Handler, Message, StreamHandler};
use actix_web::{web, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use actix::Running;
use auth::controller::{Controller, Jwt};
use database::queries::*;
use database::conn::create_db_conn_pool;
use modules::chat::db::*;
use modules::chat::structs::*;
use modules::chat::web::*;
use modules::invitation::db::*;
use modules::invitation::structs::*;
use modules::invitation::web::*;
use constants::*;



pub mod constants;

pub mod database{
    pub mod conn;
    pub mod tb;
    pub mod idx;
    pub mod queries;
}

pub mod auth{
    pub mod jwt{
        pub mod refresh;
        pub mod structs;
        pub mod token;
    }

    pub mod controller;
}

pub mod modules {
    pub mod chat{
        pub mod db;
        pub mod structs;
        pub mod web;
    }
    pub mod invitation{
        pub mod db;
        pub mod structs;
        pub mod web;
    }
}

pub mod socket{
    pub mod types;
    pub mod manager;
    pub mod session;
}
pub mod health;
use health::*;
use socket::manager::*;
use socket::types::*;
use socket::session::*;


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let pool = create_db_conn_pool().await.unwrap();
    println!("Server started");

    let app_state = Data::new(
        AppState{
            db_pool: pool,
            web_socket_manager: WebSocketManager::new(),
        });
    
    HttpServer::new(move || {
        App::new()
            .wrap(
                // cors bedzie potem zmieniany 
                Cors::default()
                    .allow_any_origin() // Określenie konkretnych źródeł: .allow_origin("http://example.com")
                    .allow_any_method() // Określenie konkretnych metod: .allowed_methods(vec!["GET", "POST"])
                    .allow_any_header() // Określenie konkretnych nagłówków: .allowed_headers(vec!["Content-Type", "Authorization"])
                    .supports_credentials() // Włączenie obsługi kredytów
                    .max_age(3600) // Określenie czasu max_age dla wyniku pre-flight
            )
            .app_data(app_state.clone())
            .service(web::resource("/chat/create").route(web::post().to(create_chat)))
            .service(web::resource("/chat/remove/{chat_id}").route(web::delete().to(remove_chat)))
            .service(web::resource("/chat/join").route(web::post().to(join_chat)))
            .service(web::resource("/chat/invitation/create").route(web::post().to(create_invitation_code)))
            .service(web::resource("/chatlist").route(web::get().to(get_chats)))
            
            .route("/auth/login", web::post().to(Controller::login))
            .route("/auth/register", web::post().to(Controller::register))
            .route("/auth/logout", web::post().to(Controller::logout))
            .route("/auth/jwt", web::get().to(Jwt::refresh_token))
            .route("/session", web::get().to(websocket_route))
            .route("/health", web::get().to(health))
    })
    .bind("0.0.0.0:5000")?
    .run().await
}



pub struct AppState{
    pub db_pool: PgPool,
    pub web_socket_manager: WebSocketManager,
}





#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct DbMsg{
    pub chat_id: String,
    pub timestamp: String,
    pub iv: String,
    pub text: String,
    pub user_id: String,
}


#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct WsMsg{
    pub chat_id: String,
    pub timestamp: String,
    pub iv: String,
    pub text: String,
    pub is_mine: bool,
}


impl DbMsg{
    pub fn from(msg: ChatMsg, user_id: String) -> Self{
        let ChatMsg {chat_id, timestamp, iv, text} = msg;
        Self{
            chat_id,
            timestamp,
            iv,
            text,
            user_id,
        }
    }
}


impl WsMsg{
    pub fn from(msg: DbMsg, u_id: String) -> Self{
        let DbMsg {chat_id, timestamp, iv, text, user_id} = msg;
        
        Self{
            chat_id,
            timestamp,
            iv,
            text,
            is_mine: user_id == u_id,
        }
    }
}


async fn get_public_key(user_id: String, pool: &PgPool) -> Result<String, ()>{
    match select_public_key(pool, user_id).await {
        Ok(row) => Ok(row.get("public_key")),
        Err(_) => Err(())
    }
}



pub async fn insert_message(pool: &PgPool, message: DbMsg) -> Result<PgQueryResult, sqlx::Error> {
    let DbMsg {chat_id, ..} = message.clone();
    let msg_str = serde_json::to_string(&message).unwrap();
    let msg_json: Value = serde_json::from_str(&msg_str).unwrap();

    sqlx::query(UPDATE_CHAT)
        .bind(&msg_json)
        .bind(&chat_id)
        .execute(pool).await
}



async fn select_public_key(pool: &PgPool, user_id: String) -> Result<PgRow, sqlx::Error> {
    sqlx::query(GET_PUBLIC_KEY)
        .bind(user_id)
        .fetch_one(pool).await
}
use crate::*;



#[derive(Debug, Serialize, Deserialize)]
pub struct ChatsPayload{
    pub chat_ids: Vec<String>,
}



#[derive(Deserialize, Serialize,Clone, Debug)]
pub struct CallOffer{
    pub r#type: String,
    pub sdp: String,
}



#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct ChatMsg{
    pub chat_id: String,
    pub timestamp: String,
    pub iv: String,
    pub text: String,
}



#[derive(Deserialize, Serialize,Clone, Debug)]
pub struct WsMessage<T: Serialize>{
    pub msg_type: String,
    pub data: T
}



impl WsMessage<CallOffer>{
    pub fn call(offer: CallOffer) -> Self{
        WsMessage{
            msg_type: "call_offer".to_string(),
            data: offer,
        }
    }
}



impl WsMessage<ChatMsg>{
    pub fn msg(msg: ChatMsg) -> Self{
        WsMessage{
            msg_type: "message".to_string(),
            data: msg,
        }
    }
}



impl WsMessage<usize>{
    pub fn rcv() -> Self{
        WsMessage{
            msg_type: "chats_rcv".to_string(),
            data: 200,
        }
    }
}



impl Message for WsMessage<ChatMsg> { type Result = (); }

impl Message for WsMessage<CallOffer>{ type Result = (); }



impl Handler<WsMessage<ChatMsg>> for WebSocketConnection {
    type Result = ();

    fn handle(&mut self, msg: WsMessage<ChatMsg>, ctx: &mut Self::Context) {
        let msg_str = serde_json::to_string(&msg).unwrap();
        ctx.text(msg_str);
    }
}



impl Handler<WsMessage<CallOffer>> for WebSocketConnection {
    type Result = ();

    fn handle(&mut self, msg: WsMessage<CallOffer>, ctx: &mut Self::Context) {
        let msg_str = serde_json::to_string(&msg).unwrap();
        ctx.text(msg_str);
    }
}
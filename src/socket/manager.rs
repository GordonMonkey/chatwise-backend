use nano_id::base64;
use crate::*;



pub struct WebSocketManager {
    pub sessions: Arc<Mutex<HashMap<String, WebSocketConnection>>>,
}



impl WebSocketManager {
    pub fn new() -> Self {
        WebSocketManager {
            sessions: Arc::new(Mutex::new(HashMap::new())),
        }
    }
}



pub async fn websocket_route(req: HttpRequest, stream: web::Payload, data: web::Data<AppState>) -> Result<HttpResponse, actix_web::Error> {
    let user_id = match Jwt.get_user_id_from_token(req.clone()){
        Ok(d) => d,
        Err(_) => return Ok(HttpResponse::Unauthorized().into()),
    };

    let sessions = data.web_socket_manager.sessions.clone();
    let pool = data.db_pool.clone();

    let chats = Arc::new(Mutex::new(HashMap::new()));
    let chat_ids = get_chat_ids(&pool, user_id.clone()).await;
    for id in chat_ids { chats.lock().unwrap().insert(id.clone(), Vec::new()); }

    let connection = WebSocketConnection {
        id: user_id.clone(),
        user_id,
        sessions: sessions.clone(),
        addr: None,
        chats,
        pool,
    };

    
    ws::start(
        connection,
        &req,
        stream
    )
}
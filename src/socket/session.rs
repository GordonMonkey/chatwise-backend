use crate::*;



#[derive(Clone, Debug)]
pub struct WebSocketConnection {
    pub id: String,
    pub user_id: String,
    pub sessions: Arc<Mutex<HashMap<String, Self>>>,

    pub addr: Option<Addr<Self>>,
    pub chats: Arc<Mutex<HashMap<String, Vec<Addr<Self>>>>>,
    pub pool: PgPool,
}



impl Actor for WebSocketConnection {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let mut sessions = self.sessions.lock().unwrap();
        let mut chats = self.chats.lock().unwrap();

        self.addr = Some(ctx.address());
        sessions.insert(self.id.clone(), self.clone());

        for (session_id, session) in sessions.iter(){
            if session_id == &self.id { continue; }
            let session_chats = &mut session.chats.lock().unwrap();
            
            // wymiana adresow
            add_addreses(&mut chats, session_chats, session); 
            add_addreses(session_chats, &mut chats, self);
        }
        /* println!("{:#?}", sessions);
        println!("{}", sessions.len());
        println!("{:#?}", chats.keys()); */
        //addr.insert(self.id.clone(), ctx.address());
        //sessions.insert(base64::<20>(), vec![session]);

        //println!("Connection added");
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        // DO ZMIANY 

        /* let sessions = self.sessions.lock().unwrap();
        let chats = self.chats.lock().unwrap();
        let chat_ids = chats.keys().cloned().collect::<Vec<String>>();

        for (_, session) in sessions.iter(){
            let mut session_chats = session.chats.lock().unwrap();
            
            for chat_id in chat_ids.iter(){
                if session_chats.contains_key(chat_id){
                    let chat = session_chats.get_mut(chat_id).unwrap();
                    let index = chat.iter().position(|addr| addr == &self.addr.clone().unwrap()).unwrap();
                    chat.remove(index);
                }
            }
        } */


        println!("Connection removed: {}", self.id);
        Running::Stop
    }

    fn stopped(&mut self, ctx: &mut Self::Context) {
        let mut sessions = self.sessions.lock().unwrap();
        
        for (_, session) in sessions.iter(){
            let mut session_chats = session.chats.lock().unwrap();
            
            for (chat_id, chat) in session_chats.iter_mut(){
                let index = chat.iter().position(|addr| addr == &self.addr.clone().unwrap());
                if let Some(index) = index {
                    chat.remove(index);
                }
            }
        }
        sessions.remove(&self.id);
    }
}



impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WebSocketConnection {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        let text = match msg {
            Ok(ws::Message::Text(text)) => text,
            _ => return,
        };
        
        let sessions = self.sessions.lock().unwrap();
        let mut chats = self.chats.lock().unwrap();
        //println!("{:#?}", chats);
        
        // todo || CHATS HANDLER 
        let chats_payload: Option<ChatsPayload> = match serde_json::from_str(&text){
            Ok(data) => Some(data),
            Err(_) => {None},
        };

        if chats_payload.is_some(){
            let payload = chats_payload.unwrap();

            let chat_ids = payload.chat_ids;
            if chat_ids.len() == 0 { 
                chats.clear();
            }

            for id in chat_ids.iter(){ 
                println!("chat_id: {}", id);
                chats.insert(id.to_string(), vec![]); 
            }
            
            for (session_id, session) in sessions.iter(){
                if session_id == &self.id { continue; }
                let session_chats = &mut session.chats.lock().unwrap();
                
                // wymiana adresow
                add_addreses(&mut chats, session_chats, session); 
                add_addreses(session_chats, &mut chats, self);
            }

            println!("{:#?}", chats);
            return;
        }

        // todo || MESSAGE HANDLER 
        let chat_msg: Option<ChatMsg> = match serde_json::from_str(&text){
            Ok(data) => Some(data),
            Err(e) => {println!("{e}");None},
        };

        if chat_msg.is_some() {
            let chat_msg = chat_msg.unwrap();
            let message = WsMessage::msg(chat_msg.clone());

            let pool = &self.pool;
            let user_id = &self.user_id;
            let msg_clone = chat_msg.clone();
            let pool_clone = pool.clone();
            let user_id_clone = user_id.clone();
            
            tokio::spawn(async move {   
                let message = DbMsg::from(msg_clone, user_id_clone);
                insert_message(&pool_clone, message).await
            });
            
            println!("{}", chats.len());

            for (chat_id, chat) in chats.iter(){
                if chat_id == &chat_msg.chat_id {
                    println!("sending to: {:?}", chat.len());
                    
                    for addr in chat.iter(){
                        if addr == &self.addr.clone().unwrap() { continue; }
                        addr.do_send(message.clone());
                    }
                }
            }


            return;
        }
/* 
        // todo || CALL HANDLER 
        let call_offer: Option<CallOffer> = match serde_json::from_str(&text){
            Ok(data) => Some(data),
            Err(e) => {println!("{e}");None},
        };


        if call_offer.is_some(){
            let offer = call_offer.unwrap();
            let message = WsMessage::call(offer.clone());

            for (chat_id, chat) in chats.iter(){
                for addr in chat.iter(){
                    if addr == &self.addr.clone().unwrap() { continue; }
                    addr.do_send(message.clone());
                }
            }


            return;
        } */
    }
}



pub fn add_addreses(
    chats: &mut HashMap<String, Vec<Addr<WebSocketConnection>>>, 
    session_chats: &mut HashMap<String, Vec<Addr<WebSocketConnection>>>, 
    session: &WebSocketConnection)
{
    let new_session_user_id = session.user_id.clone();
    println!("new_session_user_id: {}", new_session_user_id);

    for (session_chat_id, _) in session_chats.clone().iter_mut(){
        if chats.contains_key(session_chat_id) {
            let chat = chats.get_mut(session_chat_id).unwrap();

            if chat.contains(&session.addr.clone().unwrap()) { continue; }
            println!("adding address");
            chat.push(session.addr.clone().unwrap());

            /* session.addr.clone().unwrap().do_send(ConnectionMsg{
                text: format!("{} conntected", connection.id.clone())
            }); */
        }
    }
}



pub async fn get_chat_ids(pool: &PgPool, user_id: String) -> Vec<String> {
    let query = format!("SELECT chat_id FROM chat_bindings WHERE user_id = $1;");
    
    let rows = sqlx::query(&query)
        .bind(user_id)
        .fetch_all(pool)
        .await
        .unwrap();

    
    rows.iter().map(|row| row.get("chat_id")).collect() 
}
use crate::*;

pub async fn health() -> HttpResponse{
    HttpResponse::Ok().into()
}
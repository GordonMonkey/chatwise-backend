use actix_web::{HttpRequest, HttpResponseBuilder};
use actix_web::{HttpResponse, web};
use serde::Deserialize;
use sqlx_postgres::PgPool;
use sqlx::Error;
use sqlx::Row;
use bcrypt::{hash, verify};
use nano_id::base64;
use actix_web::cookie::{time, Cookie, SameSite};
use crate::AppState;
use crate::database::queries::{INSERT_USER, LOGIN};
use super::jwt::token::{JWT_EXPIRATION_DURATION, JWT_REFRESH_DURATION};


const PASSWORD_STRENGTH: u32 = 10;
pub struct Jwt;
pub struct Controller;


impl Controller{
    pub async fn login(
        app_state: web::Data<AppState>, 
        payload: web::Json<AuthPayload> 

    ) -> HttpResponse {
        let AuthPayload { username, password } = payload.into_inner();
        let pool = &app_state.db_pool;

        let (db_password, user_id) = match get_password(pool, username).await{
            Ok(password) => password,
            Err(_) => return HttpResponse::Unauthorized().into()
        };

        match login(db_password, password).await{
            true => authorized_response(user_id, HttpResponse::Ok()),
            false => HttpResponse::Unauthorized().into(),
        }
    }
    

    pub async fn register(
        app_state: web::Data<AppState>,
        payload: web::Json<RegisterPayload>

    ) -> HttpResponse {
        let pool = &app_state.db_pool;
        let payload = payload.into_inner();

        match insert_user(pool, payload).await{
            Ok(user_id) => authorized_response(user_id, HttpResponse::Created()),
            Err(e) => HttpResponse::Conflict().json(e.to_string())
        }
    }


    pub async fn logout() -> HttpResponse{
        let access_cookie = Cookie::build("auth_token", "")
            .path("/")
            .http_only(true)
            .same_site(SameSite::Strict)
            .max_age(time::Duration::days(0))
            .finish();
    
        let refresh_cookie = Cookie::build("refresh_token", "")
            .path("/")
            .http_only(true)
            .same_site(SameSite::Strict)
            .max_age(time::Duration::days(0))
            .finish();
    
        HttpResponse::Ok()
            .cookie(access_cookie)
            .cookie(refresh_cookie)
            .finish()
    }
}


#[derive(Deserialize, Debug, Clone)]
pub struct RegisterPayload{
    pub username: String,
    pub password: String,
    pub public_key: String,
}


#[derive(Deserialize)]
pub struct AuthPayload{
    pub username: String,
    pub password: String,
}


async fn insert_user(pool: &PgPool, data: RegisterPayload) -> Result<String, Error>{
    let RegisterPayload { username, password, public_key } = data;
    
    let uuid = base64::<64>();
    let password_hash = match hash(password, PASSWORD_STRENGTH) { 
        Ok(hashed) => hashed, 
        Err(_) => "".to_string()
    };

    sqlx::query(INSERT_USER)
        .bind(username)
        .bind(password_hash)
        .bind(uuid.clone())
        .bind(public_key)
        .execute(pool)
        .await?;

    Ok(uuid)
}


async fn get_password(pool: &PgPool, username: String) -> Result<(String, String), ()>{
    let row = match sqlx::query(LOGIN)
        .bind(username)
        .fetch_one(pool).await{
            Ok(row) => row,
            Err(_) => return Err(())
        };

    let password: String = row.get("password");
    let id: String = row.get("user_id");

    Ok((password, id))
}


async fn login(db_password: String, password: String) -> bool{
    match verify(password, &db_password) {
        Ok(valid) => valid,
        _=> false,
    }
}


pub fn authorized_response(user_id: String, mut response_builder: HttpResponseBuilder) -> HttpResponse {
    let access_token = Jwt::create(user_id.clone(), JWT_EXPIRATION_DURATION);  // 15 minut
    let refresh_token = Jwt::create(user_id.clone(), JWT_REFRESH_DURATION);  //  7 dni

    let access_cookie = Cookie::build("auth_token", access_token)
        .path("/")
        .http_only(true)
        .same_site(SameSite::None)
        .max_age(time::Duration::days(7))
        .secure(true)
        .finish();

    let refresh_cookie = Cookie::build("refresh_token", refresh_token)
        .path("/")
        .http_only(true)
        .same_site(SameSite::None)
        .max_age(time::Duration::days(7))
        .secure(true)
        .finish();

    response_builder
        .cookie(access_cookie)
        .cookie(refresh_cookie)
        .finish()
}



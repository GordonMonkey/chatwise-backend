use jsonwebtoken::{Algorithm, Header, Validation, encode, decode, DecodingKey, EncodingKey};
use std::time::Duration;
use actix_web::HttpRequest;
use chrono::Utc;
use actix_web::HttpResponse;
use super::structs::JwtResponse;
use crate::auth::controller::Jwt;


pub const JWT_EXPIRATION_DURATION: u64 = 30; // 15 min
pub const JWT_REFRESH_DURATION: u64 = 60 * 60 * 24 * 7; // 7 days


impl JwtResponse{
    pub fn unauthorized() -> JwtResponse {
        JwtResponse {
            user_id: String::from(""),
            error_status: Some(401),
            exp: 0, 
        }
    }
}


impl Jwt{    
    pub fn create(user_id: String, exp: u64) -> String {
        let jwt_secret_key = Self::read_env();
        let key = EncodingKey::from_secret(jwt_secret_key.as_ref());
    
        let expiration = Utc::now() + Duration::from_secs(exp);
        let expiration_timestamp = expiration.timestamp();
    
        encode(&Header::new(Algorithm::HS256), &JwtResponse {
            user_id,
            error_status: None,
            exp: expiration_timestamp, 
        }, &key).unwrap()
    }


    pub fn get_user_id_from_token(&self, req: HttpRequest) -> Result<String, bool> {
        if let Some(cookie) = req.cookie("auth_token") {
            let token = cookie.value();
            
            let jwt_response = match self.validate_token(token) {
                Ok(data) => data,
                Err(_) => return Err(false)
            };

            Ok(jwt_response.user_id)

        } else {
            Err(false) 
        }
    }

    pub fn check_auth(&self, request: HttpRequest) -> Result<String, HttpResponse>{
        match self.get_user_id_from_token(request) {
            Ok(user_id) => Ok(user_id),
            Err(_) => Err(HttpResponse::Unauthorized().into()),
        }
    }


    fn decode(&self, token: &str) -> Result<JwtResponse, JwtResponse> {
        let jwt_secret_key = Self::read_env();
        let key = DecodingKey::from_secret(jwt_secret_key.as_ref());

        let decoded =  match decode::<JwtResponse>(&token, &key, &Validation::new(Algorithm::HS256)){
            Ok(data) => data,
            Err(_) => return Err(JwtResponse::unauthorized())
        };
        
        Ok(decoded.claims)
    }


    pub fn validate_token(&self, token: &str) -> Result<JwtResponse, ()>{
        let jwt_response = match self.decode(token) {
            Ok(data) => data,
            Err(_) => return Err(())  
        };

        let exp_timestamp = jwt_response.exp;
        let now = Utc::now().timestamp();

        if exp_timestamp < now {
            return Err(())
        }

        Ok(jwt_response)
    }


    fn read_env() -> String{
        dotenv::var("JWT_SECRET_KEY").unwrap()
    }
}

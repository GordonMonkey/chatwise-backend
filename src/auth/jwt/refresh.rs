use actix_web::{HttpRequest, web, HttpResponse};
use sqlx::{pool, PgPool};
use crate::{auth::controller::{authorized_response, Jwt}, AppState, GET_USER};


impl Jwt{
    pub async fn refresh_token(request: HttpRequest, app_state: web::Data<AppState>) -> HttpResponse{
        let refresh_token = match request.cookie("refresh_token") {
            Some(cookie) => cookie.value().to_string(),
            None => return HttpResponse::Unauthorized().into(),
        };
        let jwt = match Jwt.validate_token(&refresh_token) {
            Ok(data) => data,
            Err(_) => return HttpResponse::Unauthorized().into(),
        };

        let user_id = jwt.user_id;

        let _ = match select_user(&app_state.db_pool, user_id.clone()).await{
            Ok(_) => (),
            Err(_) => return HttpResponse::NotFound().into(),
        };

        println!("refreshed");
        authorized_response(user_id, HttpResponse::Ok())
    }
}



async fn select_user(pool: &PgPool, user_id: String) -> Result<sqlx::postgres::PgRow, sqlx::Error> {
    let user = sqlx::query(GET_USER)
        .bind(user_id)
        .fetch_one(pool)
        .await?;
    Ok(user)
}
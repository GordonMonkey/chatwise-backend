use crate::*;



pub async fn create_chat(request: HttpRequest, payload: web::Json<ChatPayload>, app_state: web::Data<AppState>) -> impl Responder{
    let user_id = match Jwt.get_user_id_from_token(request){
        Ok(d) => d,
        Err(_) => return HttpResponse::Unauthorized().into(),
    };

    let chat = Chat::new(payload.into_inner());

    match insert_chat(&app_state.db_pool, chat.clone(), user_id).await{
        Ok(_) => HttpResponse::Created().json(chat),
        Err(e) => HttpResponse::Conflict().json(e.to_string()),
    }
}



pub async fn get_chats(request: HttpRequest, app_state: web::Data<AppState>) -> impl Responder{
    let user_id = match Jwt.get_user_id_from_token(request){
        Ok(d) => d,
        Err(_) => return HttpResponse::Unauthorized().into(),
    };

    let rows = match select_chats(&app_state.db_pool, user_id.clone()).await {
        Ok(rows) => rows,
        Err(e) => {
            println!("{:?}", e);
            return HttpResponse::InternalServerError().into();
        }
    }; 

    let mut chat_list = Vec::new();

    for row in rows{
        let chat = Chat::from(row, &user_id, &app_state.db_pool).await;
        chat_list.push(chat);    
    }

    HttpResponse::Ok().json(chat_list)
}



pub async fn join_chat(payload: web::Json<JoinChatPaload>, request: HttpRequest, app_state: web::Data<AppState>) -> impl Responder {
    let JoinChatPaload {invitation_code} = payload.into_inner();
    
    let user_id = match Jwt.get_user_id_from_token(request){
        Ok(d) => d,
        Err(_) => return HttpResponse::Unauthorized().into(),
    };

    let invitation = match get_invitation(&app_state.db_pool, &invitation_code).await{
        Ok(row) => Invitation::from(row),
        Err(_) => return HttpResponse::NotFound().into(),
    };

    let invitation = match invitation {
        Ok(inv) => inv,
        Err(e) => return HttpResponse::InternalServerError().json(e.to_string()),
    };

    if invitation.exp_date < Utc::now() || invitation.was_used {
        return HttpResponse::Gone().into();
    }

    match chat_query(&app_state.db_pool, user_id.clone(), invitation.chat_id.clone(), ADD_USER_ID_TO_CHAT).await{
        Ok(_) => {
            change_status_task__(&app_state.db_pool, invitation_code);
            match chat_query(&app_state.db_pool, user_id.clone(), invitation.chat_id, GET_CHAT).await{
                Ok(row) => HttpResponse::Created().json(Chat::from(row, &user_id, &app_state.db_pool).await),
                Err(_) => return HttpResponse::NotFound().into(),
            }
        },
        Err(e) => HttpResponse::InternalServerError().json(e.to_string()),
    }
}



pub async fn remove_chat(request: HttpRequest, path: web::Path<String>, app_state: web::Data<AppState>) -> impl Responder{
    let chat_id = path.into_inner();
    let user_id = match Jwt.get_user_id_from_token(request){
        Ok(d) => d,
        Err(_) => return HttpResponse::Unauthorized().into(),
    };

    match chat_query(&app_state.db_pool, user_id, chat_id, REMOVE_CHAT).await{
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(e) => HttpResponse::InternalServerError().json(e.to_string()),
    }
}
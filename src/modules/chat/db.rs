use crate::*;



pub async fn insert_chat(pool: &PgPool, chat: Chat, user_id: String) -> Result<PgQueryResult, sqlx::Error>{
    let messages_str = serde_json::to_string(&chat.messages).unwrap();
    let messages_json: Value = serde_json::from_str(&messages_str).unwrap();

    sqlx::query(INSERT_CHAT)
        .bind(&chat.chat_id)
        .bind(&vec![messages_json])
        .bind(&chat.chat_name)
    .execute(pool).await?;

    sqlx::query(INSERT_CHAT_BINDING)
        .bind(&user_id)
        .bind(&chat.chat_id)
    .execute(pool).await
} 



pub async fn select_chats(pool: &PgPool, user_id: String) -> Result<Vec<PgRow>, sqlx::Error> {
    sqlx::query(GET_ALL_CHATS)
        .bind(user_id)
        .fetch_all(pool).await
}

pub async fn select_all_chat_users(pool: &PgPool, user_id: String, chat_id: String) -> Result<Vec<PgRow>, sqlx::Error> {
    sqlx::query(GET_CHAT_USERS)
        .bind(user_id)
        .bind(chat_id)
        .fetch_all(pool).await
}


pub async fn chat_query(pool: &PgPool, user_id: String, chat_id: String, query: &str) -> Result<PgRow, sqlx::Error>{
    sqlx::query(query)
        .bind(user_id)
        .bind(chat_id)
        .fetch_one(pool).await
}
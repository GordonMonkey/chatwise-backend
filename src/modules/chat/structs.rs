use crate::*;



#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct JoinChatPaload {
    pub invitation_code: String,
}



#[derive(Deserialize, Serialize, Debug)]
pub struct ChatPayload{
    pub chat_name: String,
}



#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct Chat{
    pub chat_id: String,
    pub chat_name: String,
    pub messages: Vec<ChatMsg>,
    pub users_count: usize,
    pub public_key: String,
}



#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct ChatResp{
    pub chat_id: String,
    pub chat_name: String,
    pub messages: Vec<WsMsg>,
    pub users_count: usize,
    pub public_key: String,
}



impl ChatResp{
    pub fn from(chat: Chat, user_id: String, messages: Vec<DbMsg>) -> Self{
        let Chat {chat_id, chat_name, messages: _, users_count, public_key} = chat;
        let messages = messages.into_iter().map(|msg| WsMsg::from(msg, user_id.clone())).collect();

        Self{
            chat_id,
            chat_name,
            messages,
            users_count,
            public_key,
        }
    }
}



impl Chat{
    pub fn new(payload: ChatPayload) -> Self{
        let ChatPayload { chat_name } = payload;
        let chat_id = base64::<15>();

        Self{
            chat_id,
            chat_name,
            messages: vec![],
            users_count: 0,
            public_key: "".to_string(),
        }
    }


    pub async fn from(row: PgRow, user_id: &String, pool: &PgPool) -> ChatResp{
        let mut messages: Vec<DbMsg> = Vec::new(); 
        let mut user_ids: Vec<String> = Vec::new();

        let values: Vec<Value> = row.get("messages");
        let chat_id: String = row.get("chat_id");
        
        let users_rows = select_all_chat_users(pool, user_id.clone(), chat_id.clone()).await.unwrap();
        let users_count = users_rows.len();
        
        if users_rows.len() > 0 {
            for row in users_rows{
                user_ids.push(row.get("user_id"));
            }    
        }        

        if values.len() > 0{
            for value in values{
                match serde_json::from_value(value){
                    Ok(msg) => messages.push(msg),
                    Err(_e) => {continue}, 
                };
            }
        }
        
        let chat = Self{
            chat_id,
            chat_name: row.get("chat_name"),    
            public_key: match users_count{
                0 => "".to_string(),
                _ => {
                    let participant_id = user_ids.into_iter().filter(|id| id != user_id).next().unwrap();
                    let public_key = match get_public_key(participant_id, pool).await{
                        Ok(key) => key,
                        Err(_) => "".to_string(),
                    };

                    public_key
                }
            },
            messages: Vec::new(),
            users_count,
        };

        ChatResp::from(chat, user_id.clone(), messages)
    }
}
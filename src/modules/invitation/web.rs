use crate::*;



pub async fn create_invitation_code(payload: web::Json<GetInvitationCodePayload>, request: HttpRequest, app_state: web::Data<AppState>) -> impl Responder{
    let GetInvitationCodePayload {chat_id} = payload.into_inner();
    
    let user_id = match Jwt.get_user_id_from_token(request){
        Ok(d) => d,
        Err(_) => return HttpResponse::Unauthorized().into(),
    };

    let chat = match chat_query(&app_state.db_pool, user_id.clone(), chat_id, GET_CHAT).await{
        Ok(row) => Chat::from(row, &user_id, &app_state.db_pool).await,
        Err(_) => return HttpResponse::NotFound().into(),
    };

    let invitation = Invitation::new(chat.chat_id, user_id);
    let inv_code = invitation.code.clone();

    match insert_invitation(&app_state.db_pool, invitation).await {
        Ok(_) => HttpResponse::Created().json(inv_code),
        Err(e) => HttpResponse::InternalServerError().json(e.to_string()),
    }
}



pub fn change_status_task__(pool: &PgPool, code: String){
    let pool = pool.clone();
    tokio::spawn(async move { 
        match change_invitation_status(&pool, code).await{
            Ok(_) => (),
            Err(e) => {println!("{:?}", e);},
        }; 
    });
}
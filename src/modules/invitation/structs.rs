use crate::*;



#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct GetInvitationCodePayload{
    pub chat_id: String,
}



#[derive(Deserialize, Serialize, Clone)]
pub struct Invitation{
    pub chat_id: String,
    pub user_id: String,
    pub code: String,
    pub exp_date: chrono::DateTime<Utc>,
    pub was_used: bool,
}



impl Invitation {
    pub fn new(chat_id: String, user_id: String) -> Self {
        let now = chrono::Utc::now();
        let exp_time = INVITE_EXP_TIME; // 1h
        let exp_date = now + chrono::Duration::seconds(exp_time);

        Self {
            user_id,
            chat_id,
            code: base64::<6>(),
            exp_date,
            was_used: false,
        }
    }

    pub fn from(row: PgRow) -> Result<Self, sqlx::Error> {
        Ok(Self {
            chat_id: row.try_get("chat_id")?,
            user_id: row.try_get("user_id")?,
            code: row.try_get("code")?,
            exp_date: row.try_get("exp_date")?,
            was_used: row.try_get("was_used")?,
        })
    }
}
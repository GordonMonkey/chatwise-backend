use crate::*;



pub async fn insert_invitation(pool: &PgPool, invitation: Invitation) -> Result<PgQueryResult, sqlx::Error>{
    let Invitation { chat_id, user_id, code, exp_date, was_used } = invitation;

    sqlx::query(INSERT_INVITATION)
        .bind(&chat_id)
        .bind(&user_id)
        .bind(&code)
        .bind(&exp_date)
        .bind(&was_used)
        .execute(pool).await
}



pub async fn get_invitation(pool: &PgPool, code: &String) -> Result<PgRow, sqlx::Error>{
    sqlx::query(GET_INVITATION)
        .bind(code)
        .fetch_one(pool).await
}



pub async fn change_invitation_status(pool: &PgPool, code: String) -> Result<PgQueryResult, sqlx::Error>{
    sqlx::query(CHANGE_INVITATION_STATUS)
        .bind(code)
        .execute(pool).await
}

pub const INVITATION_IDX : &str = "
    CREATE INDEX IF NOT EXISTS invitation_idx ON invitations (chat_id, user_id, code);
";

pub const CHAT_IDX : &str = "
    CREATE INDEX IF NOT EXISTS chat_idx ON chats (chat_id);
";

pub const USER_IDX : &str = "
    CREATE INDEX IF NOT EXISTS user_idx ON users (user_id);
";

pub const CHAT_BINDINGS_IDX: &str = "
    CREATE INDEX IF NOT EXISTS chat_bindings_idx ON chat_bindings (chat_id, user_id);
";
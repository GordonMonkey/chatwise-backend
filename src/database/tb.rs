pub const USERS_TB: &str = "
    CREATE TABLE IF NOT EXISTS users (
        id SERIAL PRIMARY KEY,
        user_id VARCHAR(255) NOT NULL UNIQUE,
        username VARCHAR(50) NOT NULL UNIQUE,
        password VARCHAR(255) NOT NULL,
        public_key VARCHAR(255) NOT NULL
    );
";

pub const CHAT_TB: &str = "
    CREATE TABLE IF NOT EXISTS chats (
        id SERIAL PRIMARY KEY,
        chat_id VARCHAR NOT NULL UNIQUE,
        chat_name VARCHAR NOT NULL,
        messages _jsonb NOT NULL
    );
";

pub const INVITATIONS_TB: &str = "
    CREATE TABLE IF NOT EXISTS invitations (
        id SERIAL PRIMARY KEY,
        chat_id VARCHAR NOT NULL,
        user_id VARCHAR NOT NULL,
        code VARCHAR NOT NULL,
        exp_date TIMESTAMPTZ NOT NULL,
        was_used BOOLEAN NOT NULL
    );
";

pub const CHAT_BINDINGS_TB: &str = "
    CREATE TABLE IF NOT EXISTS chat_bindings (
        id SERIAL PRIMARY KEY,
        chat_id VARCHAR NOT NULL,
        user_id VARCHAR NOT NULL
    );
";
use dotenv::dotenv;
use sqlx::postgres::{PgPool, PgPoolOptions};
use std::env;
use futures::{self};
use super::tb::*;
use super::idx::*;


pub async fn create_db_conn_pool() -> Result<PgPool, Box<dyn std::error::Error>> {
    dotenv().ok();

    let host = env::var("DATABASE_HOST")?;
    let port: u16 = env::var("DATABASE_PORT")?.parse()?;
    let user = env::var("DATABASE_USER")?;
    let password = env::var("DATABASE_PASSWORD")?;
    let dbname = env::var("DATABASE_NAME")?;
    let max_cons: u32 = env::var("MAX_CONS")?.parse()?;
    
    let pool = PgPoolOptions::new()
        .max_connections(max_cons)
        .connect(&format!(
            "postgresql://{}:{}@{}:{}/{}",
            user, password, host, port, dbname
        ))
        .await?;

    let mut futures = vec![];
    for _ in 0..max_cons {
        let pool_clone = pool.clone();
        futures.push(tokio::spawn(async move {
            let _ = sqlx::query("SELECT 1")
                .fetch_one(&pool_clone)
                .await;
        }));
    }

    futures::future::join_all(futures).await;
    create_tables(&pool).await?;

    Ok(pool)
}



pub async fn create_tables(conn: &PgPool) -> Result<(), sqlx::Error> {
    sqlx::query("DROP TABLE IF EXISTS chats")
        .execute(conn)
        .await?;
    /* sqlx::query("DROP TABLE IF EXISTS users")
        .execute(conn)
        .await?; */
    sqlx::query("DROP TABLE IF EXISTS invitations")
        .execute(conn)
        .await?;
    sqlx::query("DROP TABLE IF EXISTS chat_bindings")
        .execute(conn)
        .await?;

    sqlx::query(INVITATIONS_TB).execute(conn).await?;
    sqlx::query(USERS_TB).execute(conn).await?;
    sqlx::query(CHAT_TB).execute(conn).await?;
    sqlx::query(CHAT_BINDINGS_TB).execute(conn).await?;

    sqlx::query(INVITATION_IDX).execute(conn).await?;
    sqlx::query(USER_IDX).execute(conn).await?;
    sqlx::query(CHAT_IDX).execute(conn).await?;
    sqlx::query(CHAT_BINDINGS_IDX).execute(conn).await?;
    
    Ok(())
}
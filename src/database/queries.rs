pub const INSERT_USER: &str = "
    INSERT INTO users (username, password, user_id, public_key) VALUES ($1, $2, $3, $4)
    RETURNING id;
";

pub const GET_USER: &str = "
    SELECT * FROM users WHERE user_id = $1; 
";

pub const LOGIN: &str = "
    SELECT user_id, password from users where username = $1;
";

pub const GET_ALL_CHATS: &str = "
    SELECT c.*
        FROM chats c
        JOIN chat_bindings b ON c.chat_id = b.chat_id
    WHERE b.user_id = $1;
";

pub const GET_CHAT_USERS: &str = "
    SELECT user_id
        FROM chat_bindings
    WHERE user_id != $1 and chat_id = $2;
";

pub const GET_CHAT: &str = "
    SELECT c.*
        FROM chats c
        JOIN chat_bindings b ON c.chat_id = b.chat_id
    WHERE b.user_id = $1 and b.chat_id = $2;
";

pub const UPDATE_CHAT: &str = "
    UPDATE chats 
    SET messages = messages || $1::jsonb
    WHERE chat_id = $2;
";

pub const INSERT_CHAT: &str = "
    INSERT INTO chats (chat_id, messages, chat_name)
    VALUES ($1, $2, $3);
";

pub const INSERT_CHAT_BINDING: &str = "
    INSERT INTO chat_bindings (user_id, chat_id)
    VALUES ($1, $2);
";

pub const INSERT_INVITATION: &str = "
    INSERT INTO invitations (chat_id, user_id, code, exp_date, was_used)
    VALUES ($1, $2, $3, $4, $5)
    RETURNING id;
";

pub const GET_INVITATION: &str = "
    SELECT * FROM invitations where code = $1;
";

pub const ADD_USER_ID_TO_CHAT: &str = "
    INSERT INTO chat_bindings (user_id, chat_id) VALUES ($1, $2) RETURNING id;
";

pub const CHANGE_INVITATION_STATUS: &str = "UPDATE invitations SET was_used = true WHERE code = $1";

pub const GET_PUBLIC_KEY: &str = "SELECT public_key FROM users WHERE user_id = $1;";

pub const REMOVE_CHAT: &str = "DELETE FROM chat_bindings WHERE user_id = $1 and chat_id = $2 RETURNING id;";